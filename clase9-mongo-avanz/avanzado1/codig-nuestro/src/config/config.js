const { connect } = require("mongoose")
const { productsModel } = require("../models/products.model")
const { cartModel } = require("../models/carts.model")
require('dotenv').config()

let url = `mongodb+srv://federico:federico1@coderexample.hjzrdtr.mongodb.net/comision39730?retryWrites=true&w=majority`
// let url = `mongodb://localhost:27017/comision39730`

const objConfig = {
    connectDB: async () =>{
        try {
            await connect(url)
            console.log('Base de datos conectada')
        } catch (error) {
            console.log(error)
        }
        // creamos un producto
        // await productsModel.create({
        //     title: 'Producto 3',
        //     description: 'descripción',                
        //     price:5500,
        //     thumbnail: 'url',
        //     stock: 150,
        //     code: '0002',
        //     status: true
        // })

        // creamos un cart
        // 643acacad483b576bdba2edb
        // await cartModel.create({
        //     products: []
        // })

        // insertar un producto a un cart
        // let cart = await cartModel.findById({_id: '643acacad483b576bdba2edb'})
        // cart.products.push({product: '6431b6e75e0e3d0f5002c183'})
        // let resp = await cartModel.findByIdAndUpdate({_id: '643acacad483b576bdba2edb'}, cart)
        

        let cart = await cartModel.find({_id: '643acacad483b576bdba2edb'})
        // let cart = await cartModel.findById({_id: '643acacad483b576bdba2edb'})

        console.log(JSON.stringify(cart, null, 2))
        // console.log(cart.products)
    }

}

module.exports = {
    objConfig
}