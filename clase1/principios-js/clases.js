// Clase -> Molde crear galletas -> crear galletas (instancia)
// clase -> objetos (instanciar)
// const persona = {
//     nombre: 'nombre'
//     saludar: ()=>{
    // console.log('')
// }
// const persona = {
//     nombre: 'nombre'
//     saludar: ()=>{
    // console.log('')
// }

// }

class Persona {
    constructor(nombre, apellido){
        this.nombre = nombre
        this.apellido = apellido
    }
    static saludo = 'Este es un saludo'
    saludar = () =>  `${this.nombre} ${this.apellido} dice Hola.`

    despedirse(){
        return `${this.nombre} ${this.apellido} dice Adeu.`
    }
}

let instancia = new Persona('Federico', 'Osandón')
let instancia2 = new Persona('Juan', 'Perez')
console.log(Persona.saludo)

console.log(instancia2.nombre)
console.log(instancia2.saludar())
console.log(instancia2.despedirse())

console.log(Date.now())
