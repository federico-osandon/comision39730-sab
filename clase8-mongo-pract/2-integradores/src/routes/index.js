const {Router} = require('express')
const userRouter = require('./user.router.js')
const productRouter = require('./product.router.js')
const { uploader } = require('../utils/uploader.js')

const router = Router()

router.use('/api/usuarios', userRouter)
router.use('/api/productos', productRouter)

router.post('/upload', uploader.single('myFile'),(req, res)=>{
    res.send('Archivo subido correctamente')
})
// router.use('/api/carrito', cartRouter)
// router.use('/api/orden', orderRouter)

module.exports = router 