import chai from 'chai'
import mongoose  from 'mongoose'
import Users from '../src/dao/Users.dao.js'

mongoose.connect('mongodb://localhost:27017/adoptame39730')
const expect = chai.expect

describe('set de test de Chai', () =>{
    before(function () {
        this.userDao = new Users
    })
    beforeEach(function () {
        // mongoose.connection.collections.users.drop()
        this.timeout(5000)
    })

    it('El dao debe poder obtener los usuarios en formato de arreglo ', async function () {
        let result = await this.userDao.get()
        console.log(result)
        expect(result).to.be.deep.equal([])
        // expect(result.pets).deep.equal([])
        // expect(Array.isArray(result)).to.be.ok
        // expect(Array.isArray(result)).to.be.equals(true)
        

    })
})
