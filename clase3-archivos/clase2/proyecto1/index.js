const moment = require('moment')


const hoy = moment()
console.log(hoy)

const fecha_nacimiento = moment('1996-07-13','YYYY-MM-DD')
if(fecha_nacimiento.isValid()){
    console.log(`Desde mi nacimiento, han pasado ${hoy.diff(fecha_nacimiento,'days')} días`);
}else{
    console.error("No se puede proseguir ya que la fecha es inválida")
}
