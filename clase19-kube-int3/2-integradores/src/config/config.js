const {connect} = require('mongoose')
const dotenv = require('dotenv')
const {commander} = require('../utils/commander.js')

const {mode} = commander.opts()
// console.log(mode)

dotenv.config({
    path: mode === 'development' ? './.env.development' : './.env.production'
})

// console.log(mode

// const url = 'mongodb+srv://federico:federico1@coderexample.hjzrdtr.mongodb.net/comision39730?retryWrites=true&w=majority'
const url = 'mongodb://localhost:27017/comision39730'

module.exports = {
    port: process.env.PORT || 8000,
    mongoURL: process.env.MONGO_URL,
    adminName: process.env.ADMIN_NAME ||'',
    adminPassword: process.env.ADMIN_PASSWORD || '',
    jwtSigned: process.env.SECRETO || '',
    persistence: process.env.PERSISTENCE,
    gmail_app_password: process.env.GMAIL_APP_PASSWORD,
    gmail_mail_user: process.env.GMAIL_MAIL_USER,
    // connectDB: ()=>{
    //     try {
    //         connect(url)
    //         console.log('Conectado a la base de datos')
    //     } catch (err) {
    //         console.log(err)
    //     }
    // }
}
 