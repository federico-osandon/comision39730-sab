// const task = (parametros) => {
//     return new Promise( ( resuelta, rechazada )=>{ // o res, rej - resulto o rechazado
//         // acciones
    
//         // resuelta( 'Todo bien' )
//         rechazada('todo mal')
//     })
// } 


// console.log(task)

// task() // pendiente, cumplido, rechazado   
//     .then(respuestaRes => {
//         // throw new Error('simulacion ')
//         console.log( respuestaRes)
//         //f  fetch
//     })
//     .catch(err => console.log(err))

const dividir = (dividendo, divisor) => {
    return new Promise( ( res, rej )=>{ // o res, rej - resulto o rechazado
            // acciones
            if(divisor === 0){
                rej( 'no puedo dividir por 0' )
    
            }else{
                res( dividendo/divisor )
            }
        
    })
}

// dividir(9,0)
//     .then(respuestaRes => {
//         return respuestaRes * 5 // 3
//     }) // 3
//     .then(res => res ** 2)
//     .then(respuesta => console.log(respuesta))
//     .catch(err => console.log(err))
//     .finally(()=> console.log('Se ejecuta siempre'))


//     // async dividir

async function funcionAsync() {
    try {
        let res = await dividir(10,0)
        console.log(res)
        
    } catch (error) {
        console.error(error)
    }
}

funcionAsync()