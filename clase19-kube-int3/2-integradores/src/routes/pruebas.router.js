const {Router} = require('express')
const { fork } = require('child_process')


const router = Router()

const operacionCompleja = (params) => {
    let result = 0
    for (let i = 0; i < 10e9; i++) {
        result += 1
        
    }
    return result
} 

router.get('/complejaBlock', (req,res) => {
    const result = operacionCompleja()
    res.send(`<center><h1>El resultado es ${result}</h1></center>`)
})

router.get('/compNoBlock', (req,res) => {
    const child = fork('./src/utils/operacionCompleja.js')
    child.send('inicia el cáclulo por favor')
    child.on('message', result => {        
        res.send(`<center><h1>El resultado es ${result}</h1></center>`)
    })
})


module.exports = router