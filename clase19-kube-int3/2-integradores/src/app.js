const express = require('express')
const routerApp = require('./routes')
// const { connectDB, port }  = require('./config/config')
const configObje  = require('./config/config')
const passport = require('passport')
// const { initializePassport } = require('./passport-jwt/strategyPassport')
// const { processFunctin } = require('./utils/process')

const app = express()
const PORT = configObje.port
// configObje.connectDB()
// console.log(configObje)

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use('/static',express.static(__dirname + '/public'))

// passport
// initializePassport()
// app.use(passport.initialize())
// processFunctin()
app.use(routerApp)

app.listen(PORT, err=>{
    if (err) {
        console.log(err)
    }
    console.log(`Server escuchando en el puerto: ${PORT}`)
})