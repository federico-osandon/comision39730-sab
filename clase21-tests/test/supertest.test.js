import chai from 'chai'
import supertest from 'supertest'

const expect = chai.expect
const requester = supertest('http://localhost:8080')

describe('testing adoptame', ()=> {

    // describe('Test de pets', ()=>{

    //     it('El endpoint  POST /api/pets debe crear una mascota correctamente', async ()=>{
    //         const petMock = {
    //             name: 'Mascotita',
    //             specie: 'Perro',
    //             birthDate: '10-10-2020'
    //         }
    
    //         const {
    //             statusCode,
    //             ok,
    //             _body
    //         } = await requester.post('/api/pets').send(petMock)
    //         console.log(statusCode)
    //         console.log(ok)
    //         console.log(_body)
    //         expect(_body.payload).to.have.property('_id')
    //         expect(_body.payload.adopted).to.equal(false)
           
    //     })
    //     it('El endpoint  POST /api/pets debe crear una mascota correctamente', async ()=>{
    //         const petMock = {
    //             // name: 'Mascotita',
    //             specie: 'Perro',
    //             birthDate: '10-10-2020'
    //         }
    
    //         const {
    //             statusCode,
    //             ok,
    //             _body
    //         } = await requester.post('/api/pets').send(petMock)
    //         console.log(statusCode)
    //         console.log(ok)
    //         console.log(_body)
            
    //         expect(statusCode).to.equal(400)
    //     })

    //     it('El metodo GET de mascotas debe obtener un array de mascota, correctamente', async ()=>{
    //         const {
    //             statusCode,
    //             ok,
    //             _body
    //         } = await requester.get('/api/pets')
    //         expect(ok).to.be.equal(true)
    //     })
    // })

    // describe('Test avanzado Session', ()=>{
    //     let cookie 
    //     it('Debe registrar un usuario correctamente', async ()=>{
    //         const userMock = {
    //             first_name: 'Fede el mejor', 
    //             last_name: 'Osandón',
    //             email: 'f@gmail.com',
    //             password: '123'
    //         }
    //         const {_body} = await requester.post('/api/sessions/register').send(userMock)
    //         console.log(_body)
    //         expect(_body.payload).to.be.ok
    //     })
    //     it('El servicio debe loguear un usuario y devolver una cookie', async ()=>{
    //         const userMock = {
    //             email: 'f@gmail.com',
    //             password: '123'
    //         }

    //         const result = await requester.post('/api/sessions/login').send(userMock)
    //         const cookieResult = result.headers['set-cookie'][0]
    //         expect(cookieResult).to.be.ok
    //         // console.log(cookieResult)
    //         cookie = {
    //             name: cookieResult.split('=')[0],
    //             value: cookieResult.split('=')[1]
    //         }
    //         expect(cookie.name).to.be.ok.and.eql('coderCookie')
    //         expect(cookie.value).to.be.ok
    //     })

    //     it('Debe enviar la cookie del usuario y desestructurar correctamente', async ()=>{
    //         const {_body} = await requester.get('/api/sessions/current').set('Cookie', [`${cookie.name}=${cookie.value}`] )
    //         expect(_body.payload.email).to.be.eql('f@gmail.com')
    //     })

    // })

    describe('Test de uploads', ()=>{
        it('Debe poder crearse una mascota con la ruta de imagen',async ()=>{
            const petMock = {
                name: 'Mascotita',
                specie: 'Perro',
                birthDate: '10-10-2020'
            }

            const result = await requester.post('/api/pets/withimage')
                .field('name', petMock.name)
                .field('specie', petMock.specie)
                .field('birthDate', petMock.birthDate)
                .attach('image', './test/coderDog.jpg')

        
                
            expect(result.status).to.be.eql(200)
            expect(result._body.payload).to.have.property('_id')
            expect(result._body.payload.image).to.be.ok
        })

    })

})
// coderCookie=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRmVkZSBlbCBtZWpvciBPc2FuZMOzbiIsInJvbGUiOiJ1c2VyIiwiZW1haWwiOiJmQGdtYWlsLmNvbSIsImlhdCI6MTY4OTQ0NTQ0MSwiZXhwIjoxNjg5NDQ5MDQxfQ.ofAudclTR8Lk_CyLsFSnGg22b93JCOnAVxqxHBKj6MU; Max-Age=3600; Path=/; Expires=Sat, 15 Jul 2023 19:24:01 GMT