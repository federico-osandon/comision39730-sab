const { createTransport } = require('nodemailer')
const { 
    gmail_app_password, 
    gmail_mail_user 
} = require('../config/config.js')

const transport = createTransport({
    service: 'gmail',
    port: 587,
    auth: {
        user: gmail_mail_user,
        pass: gmail_app_password
    }
})

let from = `email de prueba <${gmail_mail_user}>`

const sendMail = async ({userMail, subject, html}) =>{
    return await transport.sendMail({
        from,
        to: userMail,
        subject,
        html
    })
}

module.exports = {
    sendMail
}