const passport = require('passport')
const {Strategy, ExtractJwt} = require('passport-jwt')

const JWTStrategy = Strategy
const ExtractJWT = ExtractJwt

// función para extraer el token de las cookies
let cookieExtractor = (req) => {
    let token = null
    if (req && req.cookies) {
        token = req.cookies('cookieToken') // key -campo
    }
    return token
}

// estrategia de passport
const initializePassport = () => {
    passport.use('jwt', new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromExtractors([cookieExtractor]),
        secretOrKey: 'pl@br@A3cr3t@'
    },async (jwt_payload, done)=>{
        try {
            return done(null, jwt_payload)
        } catch (error) {
            return done(error)
        }
    }))
}

module.exports = {
    initializePassport
}