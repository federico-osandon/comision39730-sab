const {connect} = require('mongoose')
const { commander } = require('../utils/commander')

const { mode } = commander.opts()
// console.log(mode)

require('dotenv').config({
    path: mode === 'development' ? './.env.development':'./.env.production'
})
// const url = 'mongodb+srv://federico:federico1@coderexample.hjzrdtr.mongodb.net/comision39730?retryWrites=true&w=majority'
const url = 'mongodb://localhost:27017/comision39750'

module.exports = {
    port: process.env.PORT || 8000,
    mongoURL: process.env.MONGO_URL,
    adminName: process.env.ADMIN_NAME ||'',
    adminPassword: process.env.ADMIN_PASSWORD || '',
    jwtSigned: process.env.SECRETO || '',
    connectDB: ()=>{
        try {
            connect(url)
            console.log('Conectado a la base de datos')
        } catch (err) {
            console.log(err)
        }
    }
}
 