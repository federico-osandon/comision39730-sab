import express, { query } from 'express'

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const arrayUsuarios = [
    { id: '1', nombre: 'nombre 1', apellido: 'apellido 1', genero: 'F' },
    { id: '2', nombre: 'nombre 2', apellido: 'apellido 2', genero: 'F' },
    { id: '3', nombre: 'nombre 3', apellido: 'apellido 3', genero: 'M' },
    { id: '4', nombre: 'nombre 4', apellido: 'apellido 4', genero: 'F' },
    { id: '5', nombre: 'nombre 5', apellido: 'apellido 5', genero: 'M' },
    { id: '6', nombre: 'nombre 6', apellido: 'apellido 6', genero: 'M' },
    { id: '7', nombre: 'nombre 7', apellido: 'apellido 7', genero: 'F' },
    { id: '8', nombre: 'nombre 8', apellido: 'apellido 8', genero: 'M' }
]

// 1 - req.params 
// GET + http://localhost:8080 + /usuario/nombre/apellido

app.get('/', (req, res)=>{  
    res.send(arrayUsuarios)
})

app.get('/:userId', (req, res)=>{  
    const { userId } = req.params

    const usuario = arrayUsuarios.find((usuario) => usuario.id === userId)

    if (!usuario) return res.status(400).send('no se encontro el usuario') 

    res.status(200).send(usuario)
})

// GET   /api/v1/users   <- devuelve todos los usuarios
// GET   /api/v2/users   <- devuelve todos los usuarios
// GET  /api/users/1   <- devuelve el usuario con id 1
// POST /api/users/  <- Vamos a crear un usuario

// GET     /api/productos/1   <- devuelve el usuario con id 1
// GET     /api/productos   <- devuelve todos los usuarios
// POST    /api/productos/  <- Vamos a crear un usuario
// PUT     /api/productos/1  <- Vamos a actualizar el usuario con id 1
// PUT  /api/productos/1  <- Vamos a eliminar el usuario con id 1

// mal
// GET  /api/getUsers 
// POST  /api/postUsers 
// GET  /api/users/obterner 
// GET  /api/users


app.post('/:userId', (req, res)=>{  
    

    res.status(200).send('post')
})

// 2-query

app.get('/api/query', (req, res)=>{
    console.log(req.query)
    const { genero } = req.query

    if (!genero || ( genero.toUpperCase() !== 'F' && genero.toUpperCase() !== 'M'  )) {
        return res.send(arrayUsuarios)
    }

    let usuariosFiltrados = arrayUsuarios.filter((usuario) => usuario.genero.toUpperCase() === genero.toUpperCase())
    // console.log(genero)
    res.send(usuariosFiltrados)
})



const port = 8080
app.listen(port, () => {
    console.log('Servidor escuchando en el puerto 8080')
})

