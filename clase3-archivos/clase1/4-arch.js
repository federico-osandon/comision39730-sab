// const fs = require('fs')

// // sincroniónico

// // fs.writeFileSync('./data.txt', 'Este es el texto a guardar', 'utf-8')
// fs.appendFileSync('./data.txt', '\n ESto es otro agregado con append', 'utf-8')
// // console.log( fs.existsSync('./data.txt') )

// const archivo = fs.readFileSync('./data.txt', 'utf-8')
// console.log(archivo)
// fs.unlinkSync('./data.txt')


// callbacks

// fs.writeFile('./data.txt', 'esto es lo que agregamos al archivo', 'utf-8', (err)=>{
//     if (err) console.log(err)
// })

// fs.readFile('./.data.txt', 'utf-8', (err, resultado) => {
//     if (err) {
//         console.log(err)     
//     }
//     console.log(resultado)

//     fs.appendFile('./.data.txt',  `\n ${resultado}`, 'utf-8', err => {
//         if (err) {
//             console.log(err)        
            
//         }   
                // mas acciones
//     })
// })

// fs.unlink('./.data.txt', (err) => {
//     if (err) {
//         console.log(err)        
        
//     } 
//     console.log('Terminó de borrar')
// })


// promesas 
const { promises: fs } = require('fs')


// fs.writeFile('./data.txt', 'Creando el y escribiendo el archivo', 'utf-8')
// .then(() => console.log('terminar de escribir el archivo'))
// .catch(err => console.log(err))


const usuarios = [
    {id: 1, name:'federico', email: 'f@gmail.com'}
]
    
const manejoArchivo = async () => {


    // await fs.writeFile('./data.json', JSON.stringify(usuarios, null, 2), 'utf-8')
    // console.log('terminó')
    
    // await fs.appendFile('./data.txt', '\n esto es el texto 2', 'utf-8')
    // console.log('terminó de agregar el texto')
    
    
    
    let resultado = await fs.readFile('./package.json', 'utf-8')
    let info = JSON.parse(resultado)
    console.log(info)
    await fs.writeFile('./info.json', JSON.stringify(info, null, 2), 'utf-8')
    console.log('terminó')

//     await fs.unlink('./data.txt' ) 
//     console.log('Archivo borrado') 
}

manejoArchivo()



