// sincrónico

console.log('Iniciando tarea - 1')
console.log('Realizando tarea - 2')
console.log('Continuando con otra operación - 3')
console.log('Tarea finalizada - 4')


// interval

console.log('Iniciando tarea - 1')
console.log('Realizando operación - 2')
for(let cont =0; cont<=5; cont++){
    console.log(cont) // mostrar el contador
}
console.log('Tarea finalizada - 3')