const { ProductDao } = require("../dao/factory")
const ProductRepository = require("../repositories/products.repository")


const productService = new ProductRepository(new ProductDao())

module.exports = {
    productService
}