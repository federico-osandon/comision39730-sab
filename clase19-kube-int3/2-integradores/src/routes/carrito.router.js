const {Router} = require('express')
const { cartModel } = require('../controllers/carts.model')

const router = Router()

router.get('/:cid', async (req, res)=> {
    const {cid} = req.params
    let carts = await cartModel.findOne({})
    res.send(carts)
})

module.exports = router