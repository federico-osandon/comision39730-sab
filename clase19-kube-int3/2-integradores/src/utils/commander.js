const {Command} = require('commander')

const commander = new Command()

commander
    .option('--mode <mode>', 'Modo de ejecución de nuestra app.', 'development')
    .parse()

module.exports = {
    commander
}