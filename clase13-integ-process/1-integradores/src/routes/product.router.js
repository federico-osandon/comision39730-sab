const { Router } = require('express')

const ProductController = require('../controllers/products.controller')
const passport = require('passport')
const { authPassport } = require('../passport-jwt/authPassport')
const { authorization } = require('../passport-jwt/authorizationPassport')

const router = Router()
const {
    getProducts, 
    getProduct, 
    createProduct,
    updateProdct,
    deleteProduct
} = new ProductController()

// router.get('/',        passport.authenticate('jwt'),getProducts)
// router.get('/',        authPassport('jwt'),authorization('admin'),getProducts)
router.get('/',        getProducts)
router.get('/:pid',    getProduct)
router.post('/',       createProduct)
router.put('/:pid',    updateProdct)
router.delete('/:pid', deleteProduct)


module.exports = router