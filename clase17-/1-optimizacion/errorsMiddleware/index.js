const { EErrors } = require("../../utils/errors/EErrors");

module.exports = (error, req, res, next)=>{
    console.log('middleware error:',error.cause)
    switch (error.code) {
        case EErrors.INVALID_TYPE_ERROR:
            res.send({status: 'error', error: error})
            break;
    
        default:
            res.send({status: 'error', error: 'Unhandled error'})
            break;
    }
}