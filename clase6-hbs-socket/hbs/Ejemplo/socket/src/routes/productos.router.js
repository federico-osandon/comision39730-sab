const express = require('express')
const { uploader } = require('../utils/multer')


const router = express.Router()


router.get('/', async (req, res)=>{    
    return res.send('get de productos')
})


router.post('/', uploader.single('file') ,async (req, res)=>{    
    const { title, thumbnail } = req.body

    return res.json({
        title,
        thumbnail
    })
})

module.exports = router


// const fs = require('fs')
// const path = require('path')

// const leerAchivo = async () => {
//     console.log('ruta: ', path.dirname(__dirname))
//     await fs.promises.readdir(path.dirname(__dirname)+'/public/uploads')
//     .then(resp => console.log(resp))
// }