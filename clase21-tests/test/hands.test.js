import chai from 'chai'
import UserDTO from '../src/dto/User.dto.js' 
import {createHash, passwordValidation} from '../src/utils/index.js'

const expect = chai.expect

describe('Testing Bcrypt', ()=>{
    it('el servivion debe devolver un hasheo efectivo para el pass', async function () {
        const password = 'pass123'
        const hashedPassword = await createHash(password)
        expect(hashedPassword).to.not.equal(password)
    })
    it('el servivion debe compararse de manera efectiva con el original', async function () {
        const password = 'pass123'
        const hashedPassword = await createHash(password)
        
        const isValid = await passwordValidation({password: hashedPassword}, password)
        expect(isValid).to.be.true
    })
    it('Si el pass se altera, debe fallar la comparación con el original', async function () {
        const password = 'pass123'
        let hashedPassword = await createHash(password)
        hashedPassword += '+'
        
        const isValid = await passwordValidation({password: hashedPassword}, password)
        expect(isValid).to.be.false
    })

})


describe('Testing del UserDTO', ()=>{
    before(function(){
        this.userDto = UserDTO
    })
    it('El DTO debe unificar el nombre y el apellido en una unica propiedad llamada name', ()=>{
        const userMock = {
            first_name: 'Fede',
            last_name: 'Elmejor',
            email: 'f@gmail.com',
            role: 'user'
        }
        const userDtoToken = UserDTO.getUserTokenFrom(userMock)

        expect(userDtoToken).to.have.property('name', 'Fede Elmejor')
    })
    it('El Dto debe eliminar las propiedades innecesarias', ()=>{
        const userMock = {
            first_name: 'Fede',
            last_name: 'Elmejor',
            email: 'f@gmail.com',
            role: 'user',
            password: 'FedeElmejor123'
        }

        const userToken = UserDTO.getUserTokenFrom(userMock)

        expect(userToken).to.not.have.property('first_name')
        expect(userToken).to.not.have.property('last_name')
        expect(userToken).to.not.have.property('password')
    })
    
})