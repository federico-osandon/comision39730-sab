// npm init-y
// npm i express

// const express = require('express')
import express from 'express'

const app = express()



// GET + http://localhost:8080 + /saludo
app.get('/bienvenida', (req, res)=>{
    res.send('<h1 style="color:blue;">Hola Mundo segundo servdior primer con express</h1>')
})

// 1 - req.params 
// GET + http://localhost:8080 + /usuario

app.get('/usuario/:nombre/:apellido', (req, res)=>{
    console.log('params:',req.params.nombre)
    const { nombre, apellido } = req.params
    res.send({nombre: nombre, apellido: apellido, email: 'p@gmail.com'})
})

// req.query 



const port = 8080
app.listen(port, () => {
    console.log('Servidor escuchando en el puerto 8080')
})

