const {Router} = require('express')
const { sendMail } = require('../utils/sendMail')

const router = Router()

router.get('/', async (req, res) => {

    let userMail = 'projectogendigital@gmail.com'
    let subject = 'Email de prueba integradora'
    let html = `<h1>Hoila</h1>`
    await sendMail({userMail, subject, html})
    res.send('email al usuario enviado')
})
router.get('/:uid', (req, res) => {
    res.send('get user be id')
})
router.post('/', (req, res) => {
    res.send('post user')
})
router.put('/:uid', (req, res) => {
    res.send('update user')
})
router.delete('/:uid', (req, res) => {
    res.send('delete user')
})

module.exports = router