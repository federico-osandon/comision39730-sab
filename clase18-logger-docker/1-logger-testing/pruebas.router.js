const {Router} = require('express')
const passport = require('passport')
const { passportCall } = require('../passport-jwt/passportCall')
const { authorization } = require('../passport-jwt/authorization.middleware')
const compression = require('express-compression')

const router = Router()

router.get('/sencilla', (req, res) => {
    let suma = 0
    for (let index = 0; index < 1000000; index++) {
        suma += index
        
    }
    res.send({suma})
})

router.get('/compleja', (req, res) => {
    let suma = 0
    for (let index = 0; index < 5e8; index++) {
        suma += index
        
    }
    res.send({suma})
})

// router.get('/current', passport.authenticate('jwt', {session: false}), (req,res)=>{
    router.get('/current', 
        passportCall('jwt'), 
        authorization('user'),
        (req,res)=>{
            res.send(req.user)
        })



router.get('*', async (req,res)=>{
    res.status(404).send('Ruta no encontrada')
})



module.exports = router