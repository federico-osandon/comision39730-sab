const { connect } = require('mongoose')
const config = require('../config/config.js')


class MongoSingleton {
    static #instance 
    constructor(){
        connect(config.mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
    }

    static getInstance(){
        if (this.#instance) {
            console.log('Ya está conectada')
            return this.#instance
        }

        this.#instance = new MongoSingleton()
        console.log('Connected')
        return this.#instance
    }
}

module.exports = MongoSingleton