const { Router } = require('express')

const ProductController = require('../controllers/products.controller')
// const passport = require('passport')
// const { authPassport } = require('../passport-jwt/authPassport')
// const { authorization } = require('../passport-jwt/authorizationPassport')
const {
    getProducts, 
    getProduct, 
    createProduct,
    updateProduct,
    deleteProduct
} = new ProductController()

const router = Router()

// router.get('/',        passport.authenticate('jwt'),getProducts)
// router.get('/',        authPassport('jwt'),authorization('admin'),getProducts)
router.get('/',        getProducts)
router.get('/:pid',    getProduct)
router.post('/',       createProduct)
router.put('/:pid',    updateProduct)
router.delete('/:pid', deleteProduct)


module.exports = router