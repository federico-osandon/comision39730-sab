const { Router } = require('express')
const productManager = require('../managersDaos/managerProductMongo')

const router = Router()


router.get('/', async (req, res) => {
    const resp = await productManager.getAllProducts()
    res.send(resp)
})
router.get('/:pid', (req, res) => {
    res.send('get product be id')
})
router.post('/', async (req, res) => {
    const newProduct = req.body
    const resp = await productManager.addProduct(newProduct)
    res.send(resp)
})
router.put('/:pid', (req, res) => {
    res.send('update product')
})
router.delete('/:pid', (req, res) => {
    res.send('delet product')
})


module.exports = router

// message - carrito - productos 
// manager product - carrito -> mongo + messages -> mongo