const express = require('express')
const cookieParser = require('cookie-parser')
const { userRouter } = require('./routes/users.router')
const productsRouter = require('./routes/productos.router')
const viewsRouter = require('./routes/views.router')
const cartRouter = require('./routes/cart.router')

const handlebars = require('express-handlebars') // importación hbs
const { Server } = require('socket.io')

const app = express()
const PORT = 4000

// habdelbars config _______________________________________________________


// habdelbars config _______________________________________________________
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// app.use('/static', express.static(__dirname + '/public'))
app.use('/virtual', express.static(__dirname + '/public'))
app.use(cookieParser())// mid Tercero


app.engine('handlebars', handlebars.engine())

app.set('views', __dirname+'/views')
app.set('view engine','handlebars' )

app.use('/vista', viewsRouter)

app.use('/api/usuarios', userRouter)
app.use('/api/productos', productsRouter)
app.use('/api/carrito', cartRouter)

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})

app.listen(PORT, (err) => {
    if (err) return console.log('Error al iniciar el servidor')

    console.log(`Servidor iniciado en el puerto ${PORT}`)
})