const mostrarLista = (arrayLista=[]) => {
    if(arrayLista.length === 0) return 'lista vacía'
    arrayLista.forEach(item => console.log(item))
    return `El tamaño de la lista es ${arrayLista.length}`
} 

console.log(mostrarLista(['Fede', 'Juan']))