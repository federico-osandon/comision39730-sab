// const express = require('express')
const express = require('express')
const cookieParser = require('cookie-parser')
const usersRouter = require('./routes/users.router.js')
const productsRouter = require('./routes/productos.router.js')
const viewsRouter = require('./routes/views.router.js')
// const { uploader } = require('./utils.js')
// handlebars_______________________________________________________________
const handlebars = require('express-handlebars')
const { uploader } = require('./utils/multerConfig.js')
// socket io _______________________________________________________________
const { Server } = require('socket.io')
// socket io _______________________________________________________________


const app = express()

const PORT = 8080

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/virtual' ,express.static(__dirname+'/public'))
app.use(cookieParser())

// handlebars_______________________________________________________________
app.engine('handlebars', handlebars.engine())
app.set('views', __dirname+'/views')
app.set('view engine', 'handlebars')
// handlebars_______________________________________________________________


app.use('/vista', viewsRouter)


// http://localhost:8080/api/usuarios
app.use('/api/usuarios',  usersRouter)

// http://localhost:8080/api/productos
app.use('/api/productos', productsRouter)

app.post('/single', uploader.single('myfile') ,(req, res)=>{
    res.status(200).json({
        mensaje: 'se a subido con éxito el archivo'
    })
})

app.use((err, req, res, next)=>{
    console.log(err)
    res.status(500).send('Todo mal')
})

const httpServer = app.listen(PORT,err =>{
    if (err)  console.log(err)
    console.log(`Escuchando en el puerto: ${PORT}`)
})

// instanciando un socket server
const socketServer = new Server(httpServer)

// socketServer.on('connection', socket => {
//     console.log('Nuevo Cliente conectado')

//     socket.on('message', dataCliente => {
//         console.log(dataCliente)
//     })



//     socket.emit('evento-para-socket-inividual', 'Este mensaje solo lo debe recibir el socket')
//     socket.broadcast.emit('evento-para-todos-menos-para-el-socket-actual', 'Este evento lo veran todos los socket conectados, mennos el actual')
//     socketServer.emit('eventos-para-todos', 'este lo reciben todos los socket conectados')

// })
const logs = []
socketServer.on('connection',socket =>{
    console.log("Connected")
    //Message1 se utiliza para la primera fase del ejercicio
    socket.on("message1",data=>{
        console.log(data)
        socketServer.emit('log',data);
    })

    //Message2 se utiliza para la parte de almacenar y devolver los logs completos.
    socket.on("message2",data=>{
        console.log(socket.id)
        logs.push({socketid:socket.id,message:data})
        socketServer.emit('log',{logs});
    })
})

