const { productModel } = require('./models/product.model.js')

class ProductDaoMongo {
    async get(limit, page){        
        let resp = await productModel.paginate({}, {limit, page, lean: true})
        // let resp = await productModel.find({})
        return resp
    }
    async getById(pid){
        return await productModel.findById(pid)        
    }
    async create(newProduct){
        return await productModel.create(newProduct) 
    }
    async update(pid, productToUpdate){
        return await productModel.findByIdAndUpdate(pid, productToUpdate, {new: true})
    }
    
    async delete(pid){
        return await productModel.findByIdAndDelete(pid)
    }
    
}

module.exports = ProductDaoMongo