const express = require('express')
const routerApp = require('./routes')
const { objConfig } = require('./config/config')

const app = express()
const PORT = 8080

objConfig.connectDB()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use('/static',express.static(__dirname + '/public'))

app.use(routerApp)

app.listen(PORT, err=>{
    if (err) {
        console.log(err)
    }
    console.log(`Server escuchando en el puerto: ${PORT}`)
})