class Contador {
    constructor(responsable){
        this.responsable = responsable
        this.contador = 0
    }
    static contadorGlobal = 0

    getResponsable = () => this.responsable

    contar = () => {
         
        this.contador++ // this.contador = contador + 1 - contador += 1
        Contador.contadorGlobal ++
    }

    getCuentaIndividual = () => this.contador

    getCuentaGlobal = () => Contador.contadorGlobal
}

// const contador2 = {

// }

let contador = new Contador('Fede')
let contador1 = new Contador('Juan')

console.log(contador.getResponsable())
// console.log(contador1.getResponsable())
console.log(contador.getCuentaIndividual())
console.log(contador.getCuentaGlobal())

contador.contar()
contador.contar()
contador.contar()

console.log(contador.getCuentaIndividual())
console.log(contador.getCuentaGlobal())


// function Contador1(){} // funcion contructor