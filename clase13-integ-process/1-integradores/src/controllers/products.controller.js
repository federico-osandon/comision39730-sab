const productManager = require('../managersDaos/managerProductMongo.js')

class ProductController {

    getProducts = async (req, res) => {
        try {
            const {limit=10, page=1} = req.query
            const resp = await productManager.getAllProducts(limit,page)
            res.send(resp)        
        } catch (error) {
            console.log(error)
        }
    }

    getProduct = async (req, res) => {
        try {        
            res.send('get product be id')
        } catch (error) {
            console.log(error)
        }
    }
    

    createProduct = async (req, res) => {
        try {        
            const newProduct = req.body
            const resp = await productManager.addProduct(newProduct)
            res.send(resp)
        } catch (error) {
            console.log(error)
        }
    }

    updateProdct =  (req, res) => {
        try {
            res.send('update product')
            
        } catch (error) {
            console.log(error)
        }
    }

    deleteProduct = (req, res) => {
        try {
            res.send('delet product')
            
        } catch (error) {
            console.log(error)
        }
    }
}



module.exports = ProductController