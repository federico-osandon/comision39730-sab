const { Schema, model } = require("mongoose")

const collection = 'carritos'

// let carts=[
//     {products: [
//         {product: 'fasjfdhashdf', _id:'fasdlkfhasdhjf', quantity:1 },
//         {product: 'fasjfdhashdf', _id:'fasdlkfhasdhjf', quantity:1 },
//         {product: 'fasjfdhashdf', _id:'fasdlkfhasdhjf', quantity:1 }
//     ]}
// ]

const CartSchema = new Schema({  
    products: [{
        product: {
            type: Schema.Types.ObjectId,
            ref: 'products'
        },
        quantity : Number
    }]
   
})

CartSchema.pre('findOne', function(){
    this.populate('products.product')
})

const cartModel = model(collection,CartSchema)

module.exports = {
    cartModel
}