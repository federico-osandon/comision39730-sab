const authSession = (req, res, next)=>{
    if (req.session?.user !== 'fede' && !req.session?.admin) {
        return res.status(401).send('error de autenticación')        
    }

    next()
}

module.exports = {
    authSession
}