// const array = [1,2,3,4,5]

// let callback = (num) => {
//     if (num % 2 === 0) { 
//         return num * 2 // 1/2 = 1  resto 1   - 2/2= 1 resto 0
        
//     } else {
//         return 'no es par'
//     }
// }

// let newArray = array.map( callback )

// console.log(newArray)

// equivalente 

let arregloPrueba = [1,2,3,4,5]

// function myMap(arreglo, callback) {
//     var nuevoArreglo = []
    
//     for (let i = 0; i < arreglo.length; i++) {
//       let nuevoValor = callback(arreglo[i])
//       nuevoArreglo.push(callback(nuevoValor))
//     }

//     return nuevoArreglo;
// }

// let nuevoArregloPocesado = myMap(arregloPrueba, numero => numero * 2)

// console.log(nuevoArregloPocesado)


// 
Array.prototype.miPropioMap = function(callback){

    var nuevoArreglo = []

    for (let i = 0; i < this.length; i++) {
      let nuevoValor = callback(this[i])
      nuevoArreglo.push(nuevoValor)
    }
    return nuevoArreglo;
  }

  let nuevosValores = arregloPrueba.miPropioMap(x=>x*2)
console.log(nuevosValores)

