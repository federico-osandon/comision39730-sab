import Stripe from "stripe";


export default class PaymentsService {
    constructor(){
        this.stripe = new Stripe(process.env.STRIPE_SECRET_KEY)
    }    
    createPaymentIntent = async (data) => {
        const paymentIntent = this.stripe.paymentIntents.create(data)
        return paymentIntent
    }
}