const {Command} = require('commander')

const commander = new Command()

commander
    // .option('-d', 'variable de debugs', false)
    // .option('-p <port>', 'Puerto para el servidor', 8080 )
    .option('--mode <mode>', 'Modo de ejecución de la app', 'development')
    // .requiredOption('-u <user>', 'Usuario utilizando app', 'No se ha pasado el user')
    // .option('-l, --letters [letters...]', 'sepecify the letters')
    .parse()

// console.log('Options: ', program.opts())
// console.log('Ramaining Arguments: ', program.args)

// node commander -d -p 3000 --mode development -u root --letters a b c
// node commander -d -p 3000 -u root 2 a 5 --letters a b c

module.exports = {
    commander
}