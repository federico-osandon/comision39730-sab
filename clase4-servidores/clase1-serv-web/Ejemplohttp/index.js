const http = require('http')
// console.log('Servidor web iniciado')

const server = http.createServer( (peticion, respuesta) => {
    // console.log(peticion)
    respuesta.end('Hola, este es mi primer servidor web con Node.js')
} )

const PORT = 8080

server.listen(PORT, () => {
    console.log('Servidor web iniciado en el puerto '+PORT)
})