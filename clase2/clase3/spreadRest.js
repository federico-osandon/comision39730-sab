const obj1 = {
    propiedad1: '1',
    propiedad2: 'c',
    propiedad3:  true
}

const obj2 = {
    propiedad1: '2',
    propiedad2: 'd',
    propiedad4: 4
}

// const propiedad1 = obj1.propiedad1 
// const propiedad2 = obj1.propiedad2

// const { propiedad1 } = obj1 
const obj3 = {...obj1, ...obj1}
console.log(obj3)

const { propiedad1, ...rest } = obj2

console.log(propiedad1)
console.log(rest)
