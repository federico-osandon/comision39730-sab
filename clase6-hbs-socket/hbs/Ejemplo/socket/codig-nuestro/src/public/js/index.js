// console.log('index')

const socket = io()

// socket.emit('message', 'Hola soy mensaje del cliente')


// socket.on('evento-para-socket-inividual', data => {
//     console.log(data)
// })
// socket.on('evento-para-todos-menos-para-el-socket-actual', data => {
//     console.log(data)
// })
// socket.on('eventos-para-todos', data => {
//     console.log(data)
// })


//Parte dos: Guardar mensajes por socketid.
const input  = document.getElementById('textbox');
const log = document.getElementById('log');
input.addEventListener('keyup',evt=>{
    if(evt.key==="Enter"){
        socket.emit('message2',input.value);
        input.value=""
    }
})
socket.on('log',data=>{
    let logs='';
    data.logs.forEach(log=>{
        logs += `${log.socketid} dice: ${log.message}<br/>`
    })
    log.innerHTML=logs;
})