productsArray = [
    {
        id:1,
        title: 'Producto ejemplo',
        description: 'Este es un producto',
        thumbnail: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg',
        price: 3500,
        code: '0001',
        stock: 100
    }
]

class ProductManager{
    constructor(){
        this.products = productsArray
    }
    addProduct(newProduct){

        // validar code
        const product = this.products.find(prod => prod.code === newProduct.code)
        if(product){
            return 'Existe el producto con este cód'
        }

        // id autoincremntal
        if (this.products.length === 0) {
            this.products.push( {id: 1, ...newProduct } )
            
        } else { // 1 - 1
            this.products.push( {id: this.products[this.product.length-1].id + 1  , ...newProduct } )
            
        }
    }

    getProducts(){
        return this.products
    }
    
    getProductById(id){
        const product = this.products.find(prod => prod.id === id)
        if (!product) {
            return 'Not found'
        }

        return product
    }
}

// module.exports = {
//     ProductMAnager
// }

const productos = new ProductManager()



// console.log(productos.getProducts())
// console.log(productos.addProduct({
//     title: 'Producto 2',
//     description: 'Este es un producto 2',
//     thumbnail: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg',
//     price: 4500,
//     code: '0001',
//     stock: 100
// }))

productos.addProduct({
    title: 'Producto 2',
    description: 'Este es un producto 2',
    thumbnail: 'https://cdn.palbincdn.com/users/31244/images/GORRA-BASICA-JUNIOR-CUSTOMIZASHOPBF10B-COLOR-ROSA-1611838353.jpg',
    price: 4500,
    code: '0002',
    stock: 100
})
console.log('todos',productos.getProducts())
console.log('por id: ',productos.getProductById())