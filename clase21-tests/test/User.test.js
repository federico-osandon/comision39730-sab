import mongoose from 'mongoose'
import Users     from '../src/dao/Users.dao.js'
import Assert from 'assert'

mongoose.connect('mongodb://localhost:27017/adoptame39730')

const assert = Assert.strict

 describe('Testing User Dao', ()=>{
    before(function(){
        this.userDao = new Users()
    })
    beforeEach(function () {
        // mongoose.connection.collections.users.drop()
        this.timeout(5000)
    })
    it('Nuestro dao debe poder obrener un array con Usuarions', async function(){
        console.log(this.userDao)
        const result = await this.userDao.get()
        assert.strictEqual(Array.isArray(result), true)
    })
    it('Nuestro dao debe poder agregar un usuario a la base de datos, correctamente', async function(){
        let mockUser = {
            first_name: 'Federico',
            last_name: 'Osandón',
            email: 'f@gmail.com',
            password: '123456',
        }
        const result = await this.userDao.save(mockUser)
        assert.ok(result._id)
        assert.deepStrictEqual(result.pets, [])
    })
    
    it('El deao puede obtener un usuario por email', async function () {
        let mockUser = {
            first_name: 'Federico',
            last_name: 'Osandón',
            email: 'f@gmail.com',
            password: '123456',
        }
        const result = await this.userDao.save(mockUser)

        let userDb = await this.userDao.getBy({email: 'fede@gmail.com'}) 
        assert.strictEqual(userDb.email, 'f@gmail.com')
    })
 })