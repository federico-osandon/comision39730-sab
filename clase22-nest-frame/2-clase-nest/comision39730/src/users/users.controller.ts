import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, HttpStatus, Query, Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { query } from 'express';
import { User } from './entities/user.entity';
import {ConfigService } from '@nestjs/config'


@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService, private config: ConfigService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    
    return this.usersService.create(createUserDto);
  }

  @Post('/:b')
  probarRequest(@Request() req){
    console.log(req.query)
    console.log(req.params)
    console.log(req.body)
    return 'Todo en un objeto'
  }

  @Get()
  async findAll() {   
    try {
      const users = await this.usersService.findAll();
      const nombre = this.config.get<string>('NOMBRE')
      // console.log(users)
      return {status: 'succsess', users, nombre}     
    } catch (error) {
      console.log(error)
    }
    
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }
  // put - patch(aletra parcialmente)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
