const temporizador = (cb) => {
    setTimeout(() => {
        //acciones
        cb()
    })
}

// cb 

let operacion = () => console.log('Realizando operación')

console.log('Iniciando tareas')
temporizador(operacion) // no bloqueante
console.log('Finalizando tareas')
