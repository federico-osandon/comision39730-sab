const config = require('../config/config.js')
const MongoSingleton = require('../utils/mongoSingleton.js')
let ProductDao


switch (config.persistence) {
    case 'MONGO':
        MongoSingleton.getInstance()
        const ProductDaoMongo = require("./mongo/prodcuts.mongo.js")
        ProductDao = ProductDaoMongo        
        break;
    case 'FILE':
               
        break;
    case 'MEMORY':
               
        break;

    default:
        // const ProductDaoMongo = require("./mongo/products.mongo")
        // ProductDao = ProductDaoMongo  
        break;
}

module.exports = {
    ProductDao
}


