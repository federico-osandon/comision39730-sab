const fs = require('fs')

const path = './files/Productos.json'

class ProductManagerFile{
    constructor(){
        this.path = path
    }

    readFile = async () => {
        try {
            const data = await fs.promises.readFile(this.path, 'utf-8')
            return JSON.parse(data)            
        } catch (error) {
            return []
        }
        
    }

    getProducts = async () => {
        try {
            return await this.readFile()
        } catch (error) {
            return 'No se hay productos'
        }
    }

    
    getProductById = async (id) => {
        try {
            const products = await this.readFile()
            return products.find(product => product.id === id)                     
        } catch (error) {
            return  new Error(error)
        }
    }
    
    
    addProduct = async (newItem) => {
        try {   
            
            const products = await this.readFile()
            // si esta no lo voy a crear 
            const productDb = products.find(product => product.code === newItem.code)
            // console.log(productDb)
            
            if (productDb) {
                return `Se encuenta el producto`
            }
    
            if(newItem.title === ''){
                return 'llenar bien los campos'
            }
    
    
            if (this.products.length === 0 ) {
                newItem.id = 1
                products.push(newItem) 
            } else {
                products =  [...this.products, {...newItem, id: this.products[this.products.length - 1].id + 1 } ]
            }

            await fs.promises.writeFile(this.path, JSON.stringify(products, null, 2), 'utf-8')

            return 'Producto agregado'
        } catch (error) {
            return new Error(error)
        }
    }
}


module.exports = ProductManagerFile