// const productManager = require('../managersDaos/managerProductMongo.js')
const { productService } = require('../service/index.js')

class ProductController {

    getProducts = async (req, res) => {
        try {
            const {limit=10, page=1} = req.query
            // const resp = await productManager.getAllProducts(limit,page)
            const resp  = await productService.getProduct()
            res.send(resp)        
        } catch (error) {
            console.log(error)
        }
    }

    getProduct = async (req, res) => {
        try {   
            const {pid} = req.params 
            const product = productService.getByIdProduct(pid)     
            res.send({
                status: 'success',
                payload: product
            })
        } catch (error) {
            console.log(error)
        }
    }
    

    createProduct = async (req, res) => {
        try {        
            const newProduct = req.body
            // const resp = await productManager.addProduct(newProduct)
            const resp = await productService.createProduct(newProduct)
            res.send(resp)
        } catch (error) {
            console.log(error)
        }
    }

    updateProduct =  async (req, res) => {
        try {
            const {pid} = req.params 
            const {body} = req 
            let resp = await productService.updateProduct(pid, body)
            res.send({
                status: 'success', 
                payload: resp
            })
            
        } catch (error) {
            console.log(error)
        }
    }

    deleteProduct = async (req, res) => {
        try {
            const {pid} = req.params
            let resp = await productService.deleteProduct(pid)
            res.send({
                status: 'success',
                payload: 'Product deleted'
            })
            
        } catch (error) {
            console.log(error)
        }
    }
}



module.exports = ProductController