const { connect } = require("mongoose")
const { productsModel } = require("../models/products.model")
const { cartModel } = require("../daos/mongo/models/carts.model")
const MongoSingleton = require("../utils/MongoSingleton")
require('dotenv').config()


// const { ordenes } = require("./ordenes")
// const { orderModel } = require("../models/orders.model")

// let url = `mongodb+srv://federico:federico1@coderexample.hjzrdtr.mongodb.net/comision39730?retryWrites=true&w=majority`
let url = `mongodb://localhost:27017/comision39750`

const objConfig = {
    persistence: process.env.PERSISTENCE,
    dbConection: async () =>  MongoSingleton.getInstance(),
    // connectDB: async () =>{
    //     try {
    //         await connect(url)
    //         console.log('Base de datos conectada')
    //     } catch (error) {
    //         console.log(error)
    //     }    
    // },
    url: 'mongodb+srv://federico:federico1@coderexample.hjzrdtr.mongodb.net/comision39730?retryWrites=true&w=majority'  
}

module.exports = {
    objConfig
}