// let variablePrueba = 0

// let prueba1 = variablePrueba || 'sin Valor'
// console.log(prueba1)


// let prueba2 = variablePrueba??"sin valor"
// console.log(prueba2)


// propiedades privadada
class Persona {

    #fullname

    constructor(nombre, apellido) {
        this.nombre = nombre
        this.apellido = apellido
        this.#fullname = `${this.nombre} ${this.apellido}`

    }

    getName() {
        return this.nombre
    }

    
    #metodoPrivado = () =>  `Solo accesible para la clase`
    getFullName() {
        // this.#metodoPrivado()
        return this.#fullname
    }
}

const persona = new Persona('John', 'Perez')
// persona.full 
console.log(persona.getFullName())