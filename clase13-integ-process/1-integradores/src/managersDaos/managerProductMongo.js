const { productModel } = require('./models/product.model.js')

class ProductManagerMongo {
    async getAllProducts(limit, page){        
        let resp = await productModel.paginate({}, {limit, page, lean: true})
        // let resp = await productModel.find({})
        return resp
    }
    async getProductById(pid){
        return 'GET PRODUCTOS'
    }
    async addProduct(newProduct){
        return await productModel.create(newProduct) 
    }
    async updateProduct(pid, productToUpdate){
        return 'UPDATE PRODUCTOS'
    }
    
    async deleteProduct(pid){
        return 'UPDATE PRODUCTOS'
    }
    
}

module.exports = new ProductManagerMongo()